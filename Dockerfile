FROM debian:bookworm-20240513-slim

RUN apt update && apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential python3 python3-requests git curl wget -y &&\
    rm -rf /var/lib/apt/lists/* &&\
    git clone https://github.com/raspberrypi/pico-sdk.git --branch master &&\
    cd pico-sdk &&\
    git submodule update --init &&\
    apt-get -yqq autoremove && apt-get -yqq autoclean && \
    rm -rf /var/lib/apt/lists/* /tmp/*

ENV PICO_SDK_PATH=/pico-sdk

RUN echo "Adding bootselBoot for flashing pico via CLI rather than bootsel button" &&\
  echo "Requires current code on pico has included pico_stdio_usb" &&\
  apt update && apt install libusb-1.0-0-dev -y &&\
  git clone https://github.com/illusiaDecafish/bootselBoot.git &&\
  cd bootselBoot &&\
  cc bootselBoot.c -o bootselBoot -I/usr/include/libusb-1.0 -L/usr/lib/arm-linux-gnueabihf -lusb-1.0 &&\
  chmod +x bootselBoot &&\
  mv bootselBoot /usr/sbin && \
  apt-get -yqq autoremove && apt-get -yqq autoclean &&\
  rm -rf /var/lib/apt/lists/* /tmp/*