# Arm GNU Embedded Raspberry Pi Pico SDK

Minimal build tools and Raspberry Pi Pico SDK

## If This Helps You, Please Star The Original Source Project
One click can help us keep providing and improving this component. If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk)

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword and the container pegging tag so that you always get the CI code and container code
that have been designed, tested and released together.

There **might** be times to mismatch the CI Component and Container Versions - but it should be rare.

```yaml
include:
  - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@<VERSION>
    inputs:
      PICOSDK_COMP_CONTAINER_TAG: <VERSION>

my-pico-build:
  extends: .rpi-pico-sdk
  script:
    - |
      echo "A super cool build for the Pi Pico"
```

where `<VERSION>` is the tag of the version that you are adopting.

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `PICOSDK_COMP_CONTAINER_TAG` | `ALWAYS_SET_THIS_INPUT_TO_YOUR_COMPONENT_VERSION` | CI Component Input     | Pegs container for a stable dependency, but allows override. You must set this as an input. |\
| `CI_COMPONENT_TRACE` | `` | CI Variable | Set to 'true' to see more verbose component output. GitLabs standard CI_DEBUG_TRACE=true also triggers this trace. |

### Multiarch

This component supports both amd64 and arm64 container architectures.

### Flashing Pico from CLI

This component adds bootselBoot for flashing pico via CLI rather than bootsel button. For this utility to work, the current code on the pico has included pico_stdio_usb.

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Working Example Code Using This Component

- [Raspberry Pi Pico W Blinky](https://gitlab.com/guided-explorations/embedded/ci-components/working-code-examples/raspberry-pi-pico-w-blinky/)
